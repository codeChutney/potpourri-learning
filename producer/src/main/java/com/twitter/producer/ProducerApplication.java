package com.twitter.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication(scanBasePackages = "com.twitter.producer")
public class ProducerApplication {

	@Autowired
	private TwitterProducer producer;

	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}

	@Bean
	CommandLineRunner kickOff() {
		return args -> {
			log.info("Application starts here.." + args);
			producer.start();
		};

	}

}
