package com.twitter.producer.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tweet {
	
	private String hashtag = "";
	private String tweeter = "";
	private String actualTweet = "";
	
	public Tweet(String hashtag, String tweeter, String actualTweet) {
		setActualTweet(actualTweet);
		setHashtag(hashtag);
		setTweeter(tweeter);
	}
	
	/**
	 * The message is of the format : 
	 * 'maddieepayne :  United States : Tue Nov 15 16:15:45 CST 2016  :  Felt so weird being back on the basketball court tonight'
	 * 
	 * @param message
	 * @return hashtag represented by message string
	 */
	public Tweet(String message) {
		if(message != null){
			if(message.contains("#")){
				setHashtag( message.substring(message.indexOf('#') + 1, message.indexOf(' ', message.indexOf('#') + 1)) );				
			}else{
				setHashtag("NO_HASH_TAG");
			}
			setTweeter( message.substring(0, message.indexOf(':')).trim() );
			setActualTweet( message.substring( message.lastIndexOf(": ") + 1 , message.length() - 1).trim());
		}
	}
}
