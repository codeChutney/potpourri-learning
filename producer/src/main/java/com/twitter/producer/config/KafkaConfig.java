package com.twitter.producer.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

@EnableKafka
public class KafkaConfig {

    @Value("${kafka.bootstrap.servers}")
	private String kafkaServer;

	@Value("${kafka.max.block.ms.config}")
	private String maxBlockMSConfig;

	@Value("${kafka.key.serializer.class}")
	private String keySerializer;

	@Value("${kafka.value.serializer.class}")
	private String valueSerializer;

	@Value("${kafka.default.topic}")
	private String defaultTopic;

	@Value("${kafka.batch.size}")
	private int batchSize;

	@Value("${kafka.linger.ms}")
	private int lingerMS;
	
    @Bean
	public Map<String, Object> producerConfigs() {

		Map<String, Object> props = new HashMap<>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, keySerializer);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, valueSerializer);
		props.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, maxBlockMSConfig);
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, batchSize);
		props.put(ProducerConfig.LINGER_MS_CONFIG, lingerMS);
		return props;
	}

	@Bean
	public ProducerFactory<Integer, String> producerFactory() {
		
		return new DefaultKafkaProducerFactory<>(producerConfigs());
	}

	@Bean
	public KafkaTemplate<Integer, String> kafkaTemplate() {
		
		return new KafkaTemplate<>(producerFactory());
	}

}