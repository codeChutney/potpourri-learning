package com.twitter.producer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import twitter4j.FilterQuery;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;

@Slf4j
@Component
public class TwitterProducer {

    @Value("${twitter.consumer.key}")
    private String twitterConsumerKey;

    @Value("${twitter.consumer.secret}")
    private String twitterConsumerSecret;

    @Value("${twitter.token}")
    private String twitterToken;

    @Value("${twitter.secret}")
    private String twitterSecret;

    @Value("${twitter.baseball.terms}")
    private String twitterBaseballTerms;

    private TwitterStream twitterStream;

    @Bean
    public ConfigurationBuilder twitterConfigs() {
        ConfigurationBuilder twitterConfig = new ConfigurationBuilder();
        twitterConfig.setOAuthConsumerKey(twitterConsumerKey);
        twitterConfig.setOAuthConsumerSecret(twitterConsumerSecret);
        twitterConfig.setOAuthAccessToken(twitterToken);
        twitterConfig.setOAuthAccessTokenSecret(twitterSecret);
        twitterConfig.setJSONStoreEnabled(true);
        twitterConfig.setIncludeEntitiesEnabled(true);
        return twitterConfig;
    }

    public void start() {
        twitterStream = new TwitterStreamFactory(twitterConfigs().build()).getInstance();
        StatusListener listener = new StatusListener() {

            @Override
            public void onStatus(Status status) {
                log.info(status.getUser().getScreenName() + "    :    " + status.getPlace().getCountry() + "   :   " +  status.getCreatedAt().toString() + "   :   "  + status.getText());
            }

            @Override
            public void onException(Exception ex) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                // TODO Auto-generated method stub
            }

		};

		twitterStream.addListener(listener);

		if (twitterBaseballTerms == null || twitterBaseballTerms.isEmpty()) {
			twitterStream.sample();
		} else {
			FilterQuery filterQuery = new FilterQuery();
			filterQuery.track(twitterBaseballTerms.split(","));
			twitterStream.filter(filterQuery);
		}
	}

}